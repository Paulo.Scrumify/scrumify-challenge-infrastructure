$(document).ready(function() {	
	   
	   initializeProperty();
	});

function initializeProperty() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
  formValidationProperty();
}	

function formValidationProperty() {	
        $("#workitemproperty-form").form({
            on: 'blur',
            fields: { 
            	value : {
                  identifier : 'value',
                  rules : [{
                     type : 'empty',
                     prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                  }]
               },
		        propertyGroup : {
		            identifier : 'pk.property.id',
		            rules : [{
		               type : 'empty',
		               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
		            }]
		         }
            },
            onSuccess : function(event) {
               event.preventDefault();             
               document.forms["workitemproperty-form"].submit( function(data){
              	 $('body').html(data);
               });
            }
       });
};


function modaladdproperty(idWorkitem) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_TAGS_CREATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + idWorkitem;
    ajaxget(url, "div[id='modal']", function() {
    	initializeProperty();
    }, true);
}

function editProperty(idProperty, idWorkitem) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_TAGS_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + idProperty
	+ [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + idWorkitem;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
    
    ajaxget(url, "div[id='modal']", function() {
    	var from = jQuery("select[name='pk.property.id']");
    	from.attr('disabled', 'disabled');
        initialize();
    }, true);
}


function deleteProperty(idProperty, idWorkitem) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_TAGS_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + idProperty
	+ [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + idWorkitem;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
    $("#modal-confirmation")
	.modal({closable: false,
   		onApprove: function() {
   			ajaxget(url, "div[id='workitemproperties']", function() {
   				initializeProperty();
   	       }, false);
   		},
   		onDeny: function() {
   		}
	})
	.modal('show');  
}
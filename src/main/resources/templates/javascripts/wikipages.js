$(document).ready(function() {

   initialize();
});
        
function initialize() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_PAGES_SAVE}}]];
   var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS}}]];

   ajaxpost(url, $("#wikipage-form").serialize(), "div[id='wikis-list']", true, function() {
      initialize();
   },returnurl);
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  title : {
            identifier : 'title',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         }, 
         content : {
            identifier : 'content',
            rules : [ {
               type : 'ckeditorvalidator',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]],
               value: "textarea[id='content']"
            } ]
         }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}

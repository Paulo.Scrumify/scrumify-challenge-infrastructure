$(document).ready(function() {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS_CREATE}}]];
   
   if (id != 0) {
      url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_UPDATE}]];
   }
   
   ajaxtab(true, url, false, function(settings) {
	      switch(settings.urlData.tab) {
	         case "contracts":
	        	 getcontracts();
	            break;
	         case "workitems":
	        	 getworkitems();
	            break;
	         case "properties":
	        	 getproperties();
	            break;	            
	         case "info":
	         default:
	        	 getinfo();
	            break;
	      }
	   });
   
   initialize();
   inactive()
});

function initialize() {
  $('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	$('.ui.avatar').popup({
	       position: 'top center'
	});

	$('.message .close')
	.on('click', function() {
	  $(this)
	    .closest('.message')
	    .transition('fade');
	});
	
	formValidation();
}

function formSubmission() {   
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS_SAVE}}]];
	
	ajaxpost(url, $("#team-form").serialize(), "", true, function() {
	   initialize();
	});
}

function formValidation() {
	   $("#team-form").form({
	      on : 'blur',
	      inline : true,
	      fields : {
	    	  name : {
	               identifier : 'name',
	               rules : [ {
	                  type : 'empty',
	                  prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	               } ]
	            },
	      	  subArea : {
	                identifier : 'subArea',
	                rules : [ {
	                   type : 'empty',
	                   prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	                } ]
	             }
	      },           	  
	      onSuccess : function(event) {
	          event.preventDefault(); 
	          formSubmission();
	      }
	   });
}


function getinfo() {
	var id = $("input[type=hidden][name='id']").val();
	var url =  [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS_AJAX}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id;
	   
	ajaxget(url, "div[data-tab='info']", function() {
	      initialize();
	}); 
};

function getcontracts() {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS_CONTRACTS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   
   ajaxget(url, "div[data-tab='contracts']", function() {           
     	initialize();   
   });
};

function getworkitems() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS_WORKITEMS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	   
	ajaxget(url, "div[data-tab='workitems']", function() {           
		initialize();   
	});
};

function getproperties() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS_PROPERTIES_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	   
	ajaxget(url, "div[data-tab='properties']", function() {           
		initialize();   
	});
};
	
function updatesubarea(id,subarea) {
	 var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).TEAM_API_MAPPING_UPDATE_SUBAREA}}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + subarea;
	 
	 ajaxget(url, "div[data-tab='info']", function() {
		 initialize();
	 });
};

function addmember(id,resource) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMMEMBERS}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + resource;
 	
	ajaxget(url, "div[id='modal']", function() {
		$('.ui.dropdown').dropdown();
		$('.ui.toggle.checkbox').checkbox();
        $('#allocation-range').range({
            min: 0,
            max: 100,
            step: 1,
            start: $('#allocation').val(),
            input: '#allocation',
            onChange: function(value) {
               $('#allocation').val(value);
            }
         });
        $('#allocation').blur(function(){
	        if($('#allocation').val()>=0 && $('#allocation').val()<=100 ){
		       	 $('#allocation-range').range({
		                min: 0,
		                max: 100,
		                step: 1,
		                input: '#allocation',
		       		 start: $('#allocation').val()
		       	 });  
	        }
	    }).blur();
        $('form').form({
            on: 'blur',
            inline : true,
            fields: {
            	allocation : {
                    identifier : 'allocation',
                    rules : [ {
                  	  type : 'integer[0..100]',
                        prompt : [[#{validation.empty.range(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_RANGE}}, 0, 100)}]]
                    } ]
                 },          
               occupation : {
                  identifier : 'occupation',
                  rules : [{
                     type : 'empty',
                     prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                  }]
               }
            },
            onSuccess : function(event) {
               event.preventDefault();
               var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMMEMBERS_SAVE}}]];
               
               ajaxpost(url, $("#teammembers-form").serialize(), "div[id='members-list']", true, function() {
            	   initialize();
               });
            }
       });
 
        
   }, true);
};	


function updatemember(id,resource) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMMEMBERS_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + resource;

	ajaxget(url, "div[id='modal']", function() {
		$('.ui.dropdown').dropdown();
		$('.ui.toggle.checkbox').checkbox();
        $('#allocation-range').range({
            min: 0,
            max: 100,
            step: 1,
            start: $('#allocation').val(),
            input: '#allocation',
            onChange: function(value) {
               $('#allocation').val(value);
            }
         });

        
        $('form').form({
            on: 'blur',
            inline : true,
            fields: {
            	allocation : {
                    identifier : 'allocation',
                    rules : [ {
                  	  type : 'integer[0..100]',
                        prompt : [[#{validation.empty.range(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_RANGE}}, 0, 100)}]]
                    } ]
                 },          
               occupation : {
                  identifier : 'occupation',
                  rules : [{
                     type : 'empty',
                     prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                  }]
               }
            },
            onSuccess : function(event) {
               event.preventDefault();
               var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMMEMBERS_SAVE}}]];
               
               ajaxpost(url, $("#teammembers-form").serialize(), "div[id='members-list']", true, function() {
            	   initialize();
               });
            }
       });
   }, true);
};	

function removemember(id, resource) {
    var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMMEMBERS_DELETE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + resource;  
    
    ajaxdelete(url, "div[id='members-list']", function() {
    	initialize();
     },false);
     
 };
 
function membertask(task){
	if(task == 'reviewer' && $("input[type=checkbox][name='approver']").is(":checked")){
		$("input[type=checkbox][name='approver']"). prop("checked", false);
		$(document.getElementById("approver")).removeClass("checked")
	}else if(task == 'approver' && $("input[type=checkbox][name='reviewer']").is(":checked")){
		$("input[type=checkbox][name='reviewer']"). prop("checked", false);
		$(document.getElementById("reviewer")).removeClass("checked")
	}
}

function addcontract(id,contract) {
		var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMCONTRACTS_SAVE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + contract;
			
		ajaxget(url, "div[id='contracts-list']", function() {
	     	initialize(); 
	    }, false);
};

function removecontract(id, contract) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMCONTRACTS_DELETE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + contract;  
	    
	ajaxdelete(url, "div[id='contracts-list']", function() {
		$('.ui.dropdown').dropdown();
	},false);
	     
};

function createworkitem(id, typesofworkitem) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMWORKITEMS}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + typesofworkitem;

	
	ajaxget(url, "div[id='modal']", function() {
		CKEDITOR.replace('description', 
				{customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
				toolbar : 'Basic',
				uiColor : '#CDD1D3'});
		$('.ui.dropdown').dropdown();
		$('select.dropdown').dropdown();
		$("#workitems-form").form({
			      on : 'blur',
			      inline : true,
			      fields : {
				      name : {
				          identifier : 'name',
				          rules : [ {
				             type : 'empty',
				             prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
				         } ]
				      },
				      estimate : {
				          identifier : 'estimate',
				          rules : [ {
				             type : 'empty',
				             prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
				          } ]
				       }
			      },
			      onSuccess : function(event) {
		               event.preventDefault();
		               
		               ajaxpost($("#workitems-form").attr("action"), $("#workitems-form").serialize(), "div[id='team-workitems']", true, function() {
		            	   initialize();
		               });
		            }
			   });		
	}, true);
};

function archive() {
  var teamid = $("input[type=hidden][name='id']").val();
  var active = $("input[name='active']").val();  
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS_ARCHIVE}}]] + '/' + teamid;

  ajaxget(url, "div[id='team-info']", function() {
    initialize(); 
    });
};

function technicalassistance() {
  var teamid = $("input[type=hidden][name='id']").val();
  var technicalassistance = $("input[name='technicalassistance']").val();  
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS_TECHNICAL_ASSISTANCE}}]] + '/' + teamid;

  ajaxget(url, "div[id='team-info']", function() {
    initialize(); 
  });
};
  
function inactive() {	
  $('.ui.checkbox').checkbox({
    onChecked: function() {
      var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS}}]] + '/active/0';

      ajaxget(url, "div[id='list-teams']", function() {
        initialize(); 
      });
    },
    onUnchecked: function() {
      var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMS}}]] + '/active/1';
      
      ajaxget(url, "div[id='list-teams']", function() {
        initialize(); 
      });
    },
  });
}
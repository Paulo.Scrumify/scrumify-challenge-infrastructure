package pt.com.scrumify.entities;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Sprint;
import pt.com.scrumify.database.entities.SprintMember;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;

public class SprintView {
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=80, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String name;
   
   @Getter
   @Setter
   @Length(max=400, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String goal;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Team team;
   
   @Getter
   @Setter
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Date estimatedStart;

   @Getter
   @Setter
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Date estimatedEnd;
   
   @Getter
   @Setter
   private Time startingDay;
   
   @Getter
   @Setter
   private Time endingDay;
   
   @Getter
   @Setter
   @Valid
   private List<SprintMember> members;
   
   @Getter
   @Setter
   @Valid
   private List<WorkItem> tasks;
   
   @Getter
   @Setter
   private Date created;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   public SprintView() {
      super();
   }
   
   public SprintView(Sprint sprint) {
      super();
      
      if (sprint != null) {
         this.setId(sprint.getId());
         this.setName(sprint.getName());
         this.setTeam(sprint.getTeam());
         this.setEstimatedEnd(sprint.getEstimatedEnd());
         this.setEstimatedStart(sprint.getEstimatedStart());
         this.setMembers(sprint.getMembers());
         this.setTasks(sprint.getTasks());
         this.setGoal(sprint.getGoal());
         this.setStartingDay(sprint.getStartingDay());
         this.setEndingDay(sprint.getEndingDay());
      }
   }
   
   public Sprint translate() {
      
      Sprint sprint = new Sprint();

      sprint.setId(this.getId());
      sprint.setName(this.getName());
      sprint.setTeam(this.getTeam());
      sprint.setEstimatedEnd(this.getEstimatedEnd());
      sprint.setEstimatedStart(this.getEstimatedStart());
      sprint.setMembers(this.getMembers());
      sprint.setTasks(this.getTasks());
      sprint.setGoal(this.getGoal());
      sprint.setStartingDay(this.getStartingDay());
      sprint.setEndingDay(this.getEndingDay());
      
      return sprint;
   }
   
   public Integer getDuration() {
      return DatesHelper.getDaysBetweenDates(this.estimatedStart, this.estimatedEnd);
   }
   
   public List<Resource> getSprintMembers(){
      List<Resource> sprintMembers = new ArrayList<>();
      for (SprintMember member : this.getMembers()) {
         sprintMembers.add(member.getResource());
      }      
      return sprintMembers;
   }
}
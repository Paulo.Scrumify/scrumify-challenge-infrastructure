package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.TypeOfApplication;

public interface TypeOfApplicationService {
   TypeOfApplication getOne(Integer id);
   TypeOfApplication save(TypeOfApplication type);
   List<TypeOfApplication> listAll();
}
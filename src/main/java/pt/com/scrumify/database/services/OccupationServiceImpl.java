package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Occupation;
import pt.com.scrumify.database.repositories.OccupationRepository;

@Service
public class OccupationServiceImpl implements OccupationService {
   @Autowired
   private OccupationRepository repository;

   @Override
   public Occupation getOne(Integer id) {
      return this.repository.getOne(id);
   }

   @Override
   public List<Occupation> getAll() {
      return this.repository.getAll();
   }

   @Override
   public Occupation save(Occupation occupation) {
      return this.repository.save(occupation);
   }   
}
package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TeamMember;
import pt.com.scrumify.database.entities.TeamMemberPK;

public interface TeamMemberService {
   void delete(TeamMember teamMember);
   TeamMember getOne(TeamMemberPK pk);
   List<TeamMember> getByTeam(Team team);
   List<TeamMember> getByResource(Resource resource);
   TeamMember save(TeamMember teamMember);
   boolean isMemberOfTeam(Resource resource, Integer team);
   boolean isApproverOrReviewer(Resource resource);
}
package pt.com.scrumify.database.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.repositories.ResourceRepository;
import pt.com.scrumify.database.repositories.TeamRepository;

@Service
public class ResourceServiceImpl implements ResourceService {
   @Autowired
   private ResourceRepository resourcesRepository;
   @Autowired
   private TeamRepository teamsRepository;

   @Override
   public Resource getOne(Integer id) {
      return this.resourcesRepository.getOne(id);
   }

   @Override
   public Resource save(Resource resource) {
      return this.resourcesRepository.save(resource);
   }

   @Override
   public Resource getByEmail(String email) {
      return this.resourcesRepository.getByEmail(email);
   }

   @Override
   public Resource getByUsername(String username) {
      return this.resourcesRepository.getByUsername(username);
   }

   @Override
   public Resource getByUsernameAndActive(String username, boolean active) {
      return this.resourcesRepository.getByUsernameAndActive(username, active);
   }

   @Override
   public List<Resource> getNonSystem() {
      return this.resourcesRepository.findByProfileSystemFalseOrderByName();
   }

   @Override
   public List<Resource> getByApproved(boolean approved) {
      return this.resourcesRepository.getByApproved(approved);
   }
   
   @Override
   public List<Resource> getNonSystemUsersOrderByProfileAndResourceName() {
      return this.resourcesRepository.getNonSystemUsersOrderByProfileAndName();
   }
   
   @Override
   public List<Resource> getByExecutiveProfile() {
      return this.resourcesRepository.getResourcesByExecutiveProfile();
   }
   
   @Override
   public List<Resource> getApproversBySubArea(SubArea subArea){
      return resourcesRepository.getApproversBySubArea(subArea);
   }
   
   @Override
   public List<Resource> getBySubArea(SubArea subArea) {
      return resourcesRepository.getBySubArea(subArea);
   }
   
   @Override
   public List<Resource> getByTeam(Team team) {
      return this.resourcesRepository.getByTeam(team);
   }
   
   @Override
   public List<Resource> getByTeams(List<Team> teams) {
      if(teams.isEmpty())
         return new ArrayList<>();
      else
         return this.resourcesRepository.getByTeams(teams);
   }
   
   @Override
   public List<Resource> findWithTimesheetsToReview(List<Team> teams) {
      if(teams.isEmpty())
         return new ArrayList<>();
      else
         return this.resourcesRepository.findWithTimesheetsToReview(teams);
   }
   
   @Override
   public List<Resource> getByTeamsAndContract(List<Team> teams, Contract contract) {
      if(teams.isEmpty())
         return new ArrayList<>();
      else
         return this.resourcesRepository.getByTeamsAndContract(teams,contract);
   }
   
   @Override
   public List<Resource> getResourcesOfTimesheetSubmit(List<Resource> resource, Integer year, Integer month, Integer day) {
      return this.resourcesRepository.getResourcesOfTimesheetSubmit(resource,year,month,day);
   }
   
   @Override
   public List<Resource> getTop3ResourcesWithTimesheetUnapproved(List<Resource> resource, Integer year, Integer month, Integer day) {
      return this.resourcesRepository.getTop3ResourcesWithTimesheetUnapproved(resource,year,month,day);
   }
   
   @Override
   public List<Resource> getResourcesWithTimesheetUnreviewed(List<Resource> resources) {
      return this.resourcesRepository.getResourcesWithTimesheetUnreviewed(resources);
   }
   
   @Override
   public List<Resource> getResourcesWithTimesheetUnapproved(List<Resource> resource, Integer year, Integer month, Integer day) {
      return this.resourcesRepository.getResourcesWithTimesheetUnapproved(resource,year,month,day);
   }
   
   @Override
   public List<Resource> getResourcesWithTimesheetApprovedByContract(List<Team> teams, Integer year, Integer month, Integer fortnight, Contract contract) {
      return resourcesRepository.getResourcesWithTimesheetApproved(month, year, fortnight, teams, contract);
   }

   @Override
   public List<Resource> getMembersOfSameTeam(Resource resource) {
      return resourcesRepository.getByTeams(teamsRepository.getByMember(resource));
   }
   
   @Override
   public List<Resource> getMembersOfSameTeamAndContract(Contract contract) {
      return resourcesRepository.getMembersOfSameTeamAndContract(contract);
   }
   public List<Resource> getByContractAndTeamMember(Resource resource) {
      return resourcesRepository.getByTeams(teamsRepository.getByMember(resource));
   }

   @Override
   public List<Resource> getByTeamExceptCurrentResource(Team team, Resource resource) {
      return resourcesRepository.getByTeamExceptCurrentResource(team, resource);
   }
}
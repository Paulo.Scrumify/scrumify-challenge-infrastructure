package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@NoArgsConstructor
public class TimesheetExportPK implements Serializable {
   private static final long serialVersionUID = -3599940924307812581L;

   @Getter
   @Setter
   @Column(name = "resource", length = 50, nullable = false)
   private String resource;

   @Getter
   @Setter
   @Column(name = "year", nullable = false)
   private Integer year;

   @Getter
   @Setter
   @Column(name = "month", nullable = false)
   private Integer month;

   @Getter
   @Setter
   @Column(name = "fortnight", nullable = false)
   private Integer fortnight;

   @Getter
   @Setter
   @Column(name = "projectcode", nullable = false)
   private String project;

   @Getter
   @Setter
   @Column(name = "taskcode", nullable = false)
   private String task;

   @Getter
   @Setter
   @Column(name = "tow", nullable = false)
   private String tow;
}
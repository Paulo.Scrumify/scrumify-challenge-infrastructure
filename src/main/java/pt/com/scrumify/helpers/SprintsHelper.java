package pt.com.scrumify.helpers;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Sprint;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.WorkItemService;

@Service
public class SprintsHelper {
   
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   public WorkItemService workItemService;
   
   public List<Resource> listOfResourcesAvailable(Sprint sprint) {
      Team team = sprint.getTeam();
     
      List<Resource> members   = sprint.getSprintMembers();
      List<Resource> resources     = this.resourcesService.getByTeam(team);
      resources.removeAll(members);
      
      return resources; 
   }
   
   public List<WorkItem> getBacklog(Sprint sprint) {
      List<WorkItem> tasks = workItemService.getWorkItemsByTeam(sprint.getTeam());
      List<WorkItem> sprintTasks = sprint.getTasks();
      tasks.removeAll(sprintTasks);
      
      return tasks; 
   }
   
   public boolean isRemote(HttpServletRequest request) {
      Map<String, String> headers = new HashMap<>();
      Enumeration<String> headerNames = request.getHeaderNames();
      
      while (headerNames.hasMoreElements()) {
         String key = headerNames.nextElement();
         String value = request.getHeader(key);
         headers.put(key, value);
      }
      
      return request.getHeader(ConstantsHelper.HTTP_REFERER) != null;
   }
   
}
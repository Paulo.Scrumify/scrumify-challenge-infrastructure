package pt.com.scrumify.helpers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.entities.ScrumifyUser;

@Service
public class ContextHelper {   
   public Team getActiveTeam() {
      Team team = null;
      
      ScrumifyUser principal = getPrincipal();
      if (principal != null) {
         team = principal.getTeam();
      }
      
      return team;
   }
   
   public void setActiveTeam(Team team) {
      ScrumifyUser principal = getPrincipal();
      if (principal != null) {
         principal.setTeam(team);
      }
   }
   
   private static ScrumifyUser getPrincipal() {
      ScrumifyUser principal = null;

      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      if (authentication != null && authentication.getPrincipal() instanceof ScrumifyUser) {
         principal = (ScrumifyUser) authentication.getPrincipal();
      }
      
      return principal;
   }
}
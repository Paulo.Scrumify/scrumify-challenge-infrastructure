package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.services.ReleaseService;
import pt.com.scrumify.entities.ReleaseView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class ReleasesController {
   @Autowired
   private ReleaseService releasesService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST RELEASES
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_RELEASES_AJAX + ConstantsHelper.MAPPING_PARAMETER_APPLICATION)
   public String list(Model model, @PathVariable int app) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RELEASES, this.releasesService.getByApplication(app));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_RELEASES;
   }

   /*
    * CREATE RELEASE IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_RELEASE_CREATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION)
   public String create(Model model, @PathVariable int app) {
      ReleaseView view = new ReleaseView();
      view.getApplication().setId(app);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RELEASE, view);

      return ConstantsHelper.VIEW_APPLICATIONS_RELEASE;
   }

   /*
    * UPDATE RELEASE IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_RELEASE_UPDATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_RELEASE)
   public String update(Model model, @PathVariable int app, @PathVariable int release) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RELEASE, this.releasesService.getOne(release));

      return ConstantsHelper.VIEW_APPLICATIONS_RELEASE;
   }
   
   /*
    * DELETE RELEASE IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_RELEASE_DELETE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_RELEASE)
   public String delete(Model model, @PathVariable int app, @PathVariable int release) {
      releasesService.deleteById(release);      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RELEASES, this.releasesService.getByApplication(app));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_RELEASES;
   }

   /*
    * SAVE RELEASE IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #view.application.id)")
   @PostMapping(value = ConstantsHelper.MAPPING_RELEASE_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RELEASE) @Valid ReleaseView view) {
      this.releasesService.save(view.translate());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RELEASES, this.releasesService.getByApplication(view.getApplication().getId()));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_RELEASES;
   }
}
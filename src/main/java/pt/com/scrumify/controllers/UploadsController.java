package pt.com.scrumify.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pt.com.scrumify.entities.UploadInfo;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.MMEHelper;

@RestController
public class UploadsController {
   @Autowired
   private MMEHelper helper;
   
   @PostMapping(value = ConstantsHelper.MAPPING_REPORTS_UPLOAD)
   public @ResponseBody String upload(MultipartHttpServletRequest request) throws IOException {
      Map<String, MultipartFile> fileMap = request.getFileMap();
      List<UploadInfo> infos = new ArrayList<>();
      
      for(MultipartFile file : fileMap.values()) {
         infos.addAll(helper.processReport(file));
      }      
      
      //Return json here
      Gson gsonBuilder = new GsonBuilder().create();

      return gsonBuilder.toJson(infos);
   }
}
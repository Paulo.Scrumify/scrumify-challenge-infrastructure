package pt.com.scrumify.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.ApplicationCredential;
import pt.com.scrumify.database.services.ApplicationCredentialService;
import pt.com.scrumify.entities.ApplicationCredentialView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class ApplicationCredentialsController {
   @Autowired
   private ApplicationCredentialService applicationsCredentialsService;

   /*
    * CREATE CREDENCIAL IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_CREDENTIAL_CREATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT)
   public String create(Model model, @PathVariable int app, @PathVariable int environment) {
      ApplicationCredentialView view = new ApplicationCredentialView();
      view.getApplication().setId(app);
      view.getEnvironment().setId(environment);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFCREDENTIAL, ConstantsHelper.TypesOfCredential.values());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CREDENTIAL, view);

      return ConstantsHelper.VIEW_APPLICATIONS_CREDENTIAL;
   }

   /*
    * UPDATE CREDENCIAL IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_CREDENTIAL_UPDATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT + ConstantsHelper.MAPPING_PARAMETER_CREDENTIAL)
   public String update(Model model, @PathVariable int app, @PathVariable int environment, @PathVariable int credential) {
      ApplicationCredential applicationCredential = applicationsCredentialsService.getOne(credential, app, environment);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFCREDENTIAL, ConstantsHelper.TypesOfCredential.values());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CREDENTIAL, new ApplicationCredentialView(applicationCredential));

      return ConstantsHelper.VIEW_APPLICATIONS_CREDENTIAL;
   }

   /*
    * SAVE CREDENCIAL IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #view.application.id)")
   @PostMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_CREDENTIAL_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CREDENTIAL) @Valid ApplicationCredentialView view) {
      this.applicationsCredentialsService.save(view.translate());

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CREDENTIALS, this.applicationsCredentialsService.getByApplicationAndEnvironment(view.getApplication().getId(), view.getEnvironment().getId()));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_CREDENTIALS;
   }

   /*
    * DELETE CREDENCIAL IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_CREDENTIAL_DELETE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT + ConstantsHelper.MAPPING_PARAMETER_CREDENTIAL)
   public String delete(Model model, @PathVariable int app, @PathVariable int environment, @PathVariable int credential) {
      this.applicationsCredentialsService.delete(new ApplicationCredential(credential));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CREDENTIALS, this.applicationsCredentialsService.getByApplicationAndEnvironment(app, environment));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_CREDENTIALS;
   }
}
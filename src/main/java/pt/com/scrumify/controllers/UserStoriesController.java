package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.UserStory;
import pt.com.scrumify.database.entities.UserStoryHistory;
import pt.com.scrumify.database.entities.UserStoryNote;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.services.ApplicationService;
import pt.com.scrumify.database.services.EpicService;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.database.services.TagService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.TypeOfWorkItemNoteService;
import pt.com.scrumify.database.services.TypeOfWorkItemService;
import pt.com.scrumify.database.services.UserStoryHistoryService;
import pt.com.scrumify.database.services.UserStoryNoteService;
import pt.com.scrumify.database.services.UserStoryService;
import pt.com.scrumify.database.services.WorkItemStatusService;
import pt.com.scrumify.entities.UserStoryFilterView;
import pt.com.scrumify.entities.UserStoryNoteView;
import pt.com.scrumify.entities.UserStoryView;
import pt.com.scrumify.entities.WorkItemView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ContextHelper;
import pt.com.scrumify.helpers.CookiesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.UserStoriesHelper;
import pt.com.scrumify.helpers.WorkItemsHelper;

@Controller
public class UserStoriesController {
   @Autowired
   private ContextHelper contextHelper;
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private UserStoriesHelper helper;
   @Autowired
   private WorkItemsHelper workitemsHelper;
   @Autowired
   private ApplicationService appService;
   @Autowired
   private EpicService epicsService;
   @Autowired
   private WorkItemStatusService itemStatusService;
   @Autowired
   private SubAreaService subAreasService;
   @Autowired
   private TagService tagsService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TypeOfWorkItemService typeOfWorkitemService;
   @Autowired
   private TypeOfWorkItemNoteService typesOfWorkItemNoteService;
   @Autowired
   private UserStoryService userStoriesService;
   @Autowired
   private UserStoryHistoryService userStoryHistoryService;
   @Autowired
   private UserStoryNoteService notesService;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);

      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_USERSTORIES_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES)
   public String list(Model model) {      
      UserStoryFilterView filter = helper.getFilterFromCookie(ConstantsHelper.COOKIE_USER_STORIES);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORIES, helper.getByFilter(filter));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);

      return ConstantsHelper.VIEW_USERSTORIES_READ;
   }

   /*
    * CREATE
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_USERSTORIES_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_CREATE)
   public String create(Model model) {
      List<Team> teams = new ArrayList<>();
      teams.add(contextHelper.getActiveTeam());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, new UserStoryView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICS, epicsService.getAllByResource(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teams);

      return ConstantsHelper.VIEW_USERSTORIES_CREATE;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_USERSTORIES_READ + "')")
   @GetMapping(value = {ConstantsHelper.MAPPING_USERSTORIES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_USERSTORY,
         ConstantsHelper.MAPPING_USERSTORIES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_TAB})
   public String update(Model model, @PathVariable int userStory) {

      UserStoryView userStoryView = new UserStoryView(userStoriesService.getOne(userStory));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userStoryView);

      return ConstantsHelper.VIEW_USERSTORIES_UPDATE;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_USERSTORIES_UPDATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_USERSTORIES_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY) @Valid UserStoryView userStoryView) {
      UserStory us = helper.save(userStoryView);

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_USERSTORIES_UPDATE + ConstantsHelper.MAPPING_SLASH + us.getId();
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_USERSTORIES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_UPDATE_TAB + ConstantsHelper.MAPPING_PARAMETER_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_TAB)
   public String updatetab(Model model, @PathVariable int userStory, @PathVariable Optional<String> tab) {

      UserStoryView userStoryView = new UserStoryView(userStoriesService.getOne(userStory));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userStoryView);
      if (tab.isPresent()) {
         switch (tab.get()) {
            case "info" :
               model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(userStoryView.getType().getWorkflow(), userStoryView.getStatus()));
               model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICS, epicsService.getByTeam(userStoryView.getTeam()));
               model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), userStoryView.getTeam()));
               model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STORYPOINTS, ConstantsHelper.STORYPOINTS);
               return ConstantsHelper.VIEW_USERSTORIES_INFO;
            case "tags" :
               return ConstantsHelper.VIEW_USERSTORIES_TAGS_LIST;
            case "workitems" :
               model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEM, typeOfWorkitemService.getAvailable());
               model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMember(ResourcesHelper.getResource()));
               return ConstantsHelper.VIEW_USERSTORIES_WORKITEMS;
            case "history" :
               model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORYHISTORY, userStoryHistoryService.getUserStoryHistorybyUserStory(userStory));
               return ConstantsHelper.VIEW_USERSTORIES_HISTORY;
            case "notes" :
               model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEMNOTE, typesOfWorkItemNoteService.getByTypeOfWorkItem(userStoryView.getType()));
               return ConstantsHelper.VIEW_USERSTORIES_NOTES_LIST;
            default :
               break;
         }
      }

      return ConstantsHelper.VIEW_USERSTORIES_UPDATE;

   }

   @ApiOperation("Mapping to add a WorkItem to an UserStory")
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_TYPEOFWORKITEM)
   public String add(Model model, @PathVariable int userStory, @PathVariable int typeOfWorkitem) {

      WorkItemView workitem = new WorkItemView();
      workitem.setType(typeOfWorkitemService.getOne(typeOfWorkitem));
      UserStory userSt = userStoriesService.getOne(userStory);
      workitem.setUserStory(userSt);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, workitem);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, appService.listAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORIES, userStoriesService.getByResource());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, userSt.getTeam());

      return ConstantsHelper.VIEW_USERSTORIES_WORKITEMS_NEW;
   }

   @PostMapping(value = ConstantsHelper.MAPPING_USERSTORIES_WORKITEM_SAVE)
   public String save(Model model, @Valid WorkItemView workitem) {
      workitemsHelper.save(workitem);

      Integer id = workitem.getUserStory().getId();
      UserStoryView view = new UserStoryView(this.userStoriesService.getOne(id));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEM, typeOfWorkitemService.getAvailable());

      return ConstantsHelper.VIEW_USERSTORIES_WORKITEMS_LIST;
   }

   @ApiOperation("Mapping to update status in UserStory.")
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_INFO_STATUS + ConstantsHelper.MAPPING_PARAMETER_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_STATE)
   public String updateStatus(Model model, @PathVariable int userStory, @PathVariable int state) {
      UserStory story = userStoriesService.getOne(userStory);
      userStoryHistoryService.save(new UserStoryHistory(story));
      UserStoryView userStoryView = new UserStoryView(story);
      userStoryView.setStatus(itemStatusService.getOne(state));
      helper.save(userStoryView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userStoryView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STORYPOINTS, ConstantsHelper.STORYPOINTS);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(userStoryView.getType().getWorkflow(), userStoryView.getStatus()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICS, epicsService.getByTeam(userStoryView.getTeam()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), userStoryView.getTeam()));

      return ConstantsHelper.VIEW_USERSTORIES_INFO;
   }

   @ApiOperation("Mapping to update story points of UserStory.")
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_INFO_STORYPOINTS + ConstantsHelper.MAPPING_PARAMETER_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_STORYPOINT)
   public String updatePoints(Model model, @PathVariable int userStory, @PathVariable int storypoint) {
      UserStory story = userStoriesService.getOne(userStory);
      userStoryHistoryService.save(new UserStoryHistory(story));
      UserStoryView userStoryView = new UserStoryView(story);
      userStoryView.setEstimate(storypoint);

      helper.save(userStoryView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userStoryView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STORYPOINTS, ConstantsHelper.STORYPOINTS);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(userStoryView.getType().getWorkflow(), userStoryView.getStatus()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICS, epicsService.getByTeam(userStoryView.getTeam()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), userStoryView.getTeam()));

      return ConstantsHelper.VIEW_USERSTORIES_INFO;
   }

   /*
    * UPDATE EPIC
    */
   @ApiOperation("Mapping to update Epic in UserStory.")
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_INFO_EPIC + ConstantsHelper.MAPPING_PARAMETER_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_EPIC)
   public String updateEpic(Model model, @PathVariable int userStory, @PathVariable int epicId) {
      UserStory story = userStoriesService.getOne(userStory);
      userStoryHistoryService.save(new UserStoryHistory(story));
      UserStoryView userStoryView = new UserStoryView(story);

      Epic epic = epicsService.getOne(epicId);
      userStoryView.setEpic(epic);
      helper.save(userStoryView);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userStoryView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STORYPOINTS, ConstantsHelper.STORYPOINTS);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(userStoryView.getType().getWorkflow(), userStoryView.getStatus()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICS, epicsService.getByTeam(userStoryView.getTeam()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), userStoryView.getTeam()));

      return ConstantsHelper.VIEW_USERSTORIES_INFO;
   }

   /*
    * UPDATE TEAM
    */
   @ApiOperation("Mapping to update Team in UserStory.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CHANGE_TEAMS + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_INFO_TEAM + ConstantsHelper.MAPPING_PARAMETER_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_TEAM)
   public String updateTeam(Model model, @PathVariable int userStory, @PathVariable int team) {
      UserStory story = userStoriesService.getOne(userStory);
      userStoryHistoryService.save(new UserStoryHistory(story));

      Team currentTeam = teamsService.getOne(team);
      story.setTeam(currentTeam);
      UserStoryView userStoryView = new UserStoryView(story);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userStoryView );
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STORYPOINTS, ConstantsHelper.STORYPOINTS);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(userStoryView.getType().getWorkflow(), userStoryView.getStatus()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICS, epicsService.getByTeam(currentTeam));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), currentTeam));

      return ConstantsHelper.VIEW_USERSTORIES_INFO;
   }

   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_INFO_FIELD + ConstantsHelper.MAPPING_PARAMETER_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_FIELD
         + ConstantsHelper.MAPPING_PARAMETER_FIELD_HEADER)
   public String updateField(Model model, @PathVariable int userStory, @PathVariable String field, @PathVariable String fieldheader) {
      UserStoryView userStoryView = new UserStoryView(userStoriesService.getOne(userStory));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userStoryView);

      return "common/fragments/fieldedit :: text(label = '" + fieldheader + "', field = '" + field
            + "', required = false, maxlength = 400,placeholder = '',columnsize = 'sixteen wide'" + ", form= 'userstoryedit-form', object="
            + ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY + ")";
   }

   @PostMapping(value = ConstantsHelper.MAPPING_USERSTORIES_INFO_FIELD_UPDATE + ConstantsHelper.MAPPING_PARAMETER_FIELD)
   public String saveField(Model model, @PathVariable String field, @Valid UserStoryView userStoryView) {
      UserStory story = userStoriesService.getOne(userStoryView.getId());
      userStoryHistoryService.save(new UserStoryHistory(story));
      UserStoryView userStoryViewOld = new UserStoryView(story);
      if ("name".equals(field)) {
         userStoryViewOld.setName(userStoryView.getName());
      } else {
         userStoryViewOld.setDescription(userStoryView.getDescription());
      }
      helper.save(userStoryViewOld);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userStoryViewOld);

      return ConstantsHelper.VIEW_USERSTORIES_INFO_FRAGMENTUPDATE + field;
   }

   /*
    * TAB TAGS - open tag modal
    */

   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_TAG_ADD + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String openModalTag(Model model, @PathVariable int id) {
      UserStoryView view = new UserStoryView(userStoriesService.getOne(id));
      List<SubArea> subAreas = this.subAreasService.listSubareasByResource(ResourcesHelper.getResource());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TAGS, tagsService.getAll(subAreas));

      return ConstantsHelper.VIEW_USERSTORIES_TAGS_MODAL;

   }

   /*
    * TAB TAGS - save tag modal
    */
   @ApiOperation("Mapping to add a new tag related to an user story.")
   @PostMapping(value = ConstantsHelper.MAPPING_USERSTORIES_TAG_SAVE)
   public String saveTag(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY) @Valid UserStoryView userStoryView) {
      UserStory userstory = userStoriesService.getOne(userStoryView.getId());
      userstory.setTags(userStoryView.getTags());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, new UserStoryView(userStoriesService.save(userstory)));

      return ConstantsHelper.VIEW_USERSTORIES_TAGS_LIST;
   }

   /*
    * Tab notes - create new note
    */
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_NOTES_NEW + "/{userstoryId}" + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String createNote(Model model, @PathVariable int userstoryId, @PathVariable int id) {

      UserStoryNote note = new UserStoryNote();
      note.setUserStory(userStoriesService.getOne(userstoryId));
      note.setTypeOfNote(typesOfWorkItemNoteService.getById(id));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORYNOTE, note);

      return ConstantsHelper.VIEW_USERSTORIES_NOTES_NEW;

   }

   /*
    * Tab notes - save note
    */
   @ApiOperation("Mapping to add a new tag related to an user story.")
   @PostMapping(value = ConstantsHelper.MAPPING_USERSTORIES_NOTES_SAVE)
   public String saveNote(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORYNOTE) @Valid UserStoryNoteView noteView) {
      notesService.save(noteView.translate());

      UserStory userStory = noteView.getUserStory();
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, new UserStoryView(userStory));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEMNOTE, typesOfWorkItemNoteService.getByTypeOfWorkItem(userStory.getType()));

      return ConstantsHelper.VIEW_USERSTORIES_NOTES_LIST;
   }
   
   /*
    * Open filter
    */
   @GetMapping(value = ConstantsHelper.MAPPING_USERSTORIES_FILTER)
   public String getFilter(Model model) {
      UserStoryFilterView filter = helper.getFilterFromCookie(ConstantsHelper.COOKIE_USER_STORIES);
      
      List<WorkItemStatus> statuses = itemStatusService.getByTypeId(TypeOfWorkItem.USERSTORY);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUSES, statuses);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      
      return ConstantsHelper.VIEW_USERSTORIES_FILTER;
   }
   
   /*
    * Save filter
    */
   @PostMapping(value = ConstantsHelper.MAPPING_USERSTORIES_FILTER)
   public String submitFilter(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER) @Valid UserStoryFilterView filter) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORIES, helper.getByFilter(filter));
      
      cookiesHelper.save(ConstantsHelper.COOKIE_USER_STORIES, ResourcesHelper.getResource(), filter);
      
      return ConstantsHelper.VIEW_USERSTORIES_READ + " :: list-userstories";
   }
}
package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.Sprint;
import pt.com.scrumify.database.entities.SprintMember;
import pt.com.scrumify.database.entities.SprintMemberPK;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.SprintMemberService;
import pt.com.scrumify.database.services.SprintService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.TimeService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.entities.SprintView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.SprintsHelper;

@Controller
public class SprintsController {
   
   @Autowired
   private DatesHelper datesHelper;
   @Autowired
   private SprintsHelper sprintsHelper;
   
   @Autowired
   public ResourceService resourceService;
   @Autowired
   public SprintService sprintService;
   @Autowired
   public SprintMemberService sprintMemberService;
   @Autowired
   public TeamService teamService;
   @Autowired
   public TimeService timeService;
   @Autowired
   public WorkItemService workItemService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINTS, sprintService.listAll());
      
      return ConstantsHelper.VIEW_SPRINTS_READ;
   }
 
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS_CREATE)
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamService.getByMember(ResourcesHelper.getResource()));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MINDATE, datesHelper.getMinDateFormatted());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MAXDATE, datesHelper.getMaxDateFormatted());
      
      return ConstantsHelper.VIEW_SPRINTS_CREATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + "/start")
   public String start(Model model, @PathVariable @Valid int id) {
      
      Sprint sprint = sprintService.getOne(id);
      sprint.setStartingDay(timeService.getToday());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprint));
      
      //send mail to team informing that the sprint begun?
      
      return ConstantsHelper.MAPPING_SPRINTS_TAB_DASHBOARD;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String details(Model model, @PathVariable @Valid int id, HttpServletRequest request ) {
      Sprint sprint = sprintService.getOne(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprint));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, sprintsHelper.getBacklog(sprint));
      
      return ConstantsHelper.VIEW_SPRINTS_UPDATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_DASHBOARD)
   public String dashboardTab(Model model, @PathVariable int id, HttpServletRequest request ) {
      Sprint sprint = sprintService.getOne(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprint));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, sprintsHelper.getBacklog(sprint));
      
      if(sprintsHelper.isRemote(request))
         return ConstantsHelper.MAPPING_SPRINTS_TAB_DASHBOARD;
      return ConstantsHelper.VIEW_SPRINTS_UPDATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_TASKS)
   public String tasksTab(Model model, @PathVariable int id, HttpServletRequest request) {
      Sprint sprint = sprintService.getOne(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprint));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, sprintsHelper.getBacklog(sprint));
      
      if(sprintsHelper.isRemote(request))
         return ConstantsHelper.MAPPING_SPRINTS_TAB_TASKS;
      return ConstantsHelper.VIEW_SPRINTS_UPDATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_MEMBERS)
   public String membersTab(Model model, @PathVariable int id, HttpServletRequest request ) {
      sprintsHelper.isRemote(request);
      Sprint sprint = sprintService.getOne(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprint));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, sprintsHelper.listOfResourcesAvailable(sprint));
      
      if(sprintsHelper.isRemote(request))
         return ConstantsHelper.MAPPING_SPRINTS_TAB_MEMBERS;
      return ConstantsHelper.VIEW_SPRINTS_UPDATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_REPORTS)
   public String reportsTab(Model model, @PathVariable int id, HttpServletRequest request ) {
      Sprint sprint = sprintService.getOne(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprint));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, sprintsHelper.getBacklog(sprint));
      
      if(sprintsHelper.isRemote(request))
         return ConstantsHelper.MAPPING_SPRINTS_TAB_REPORTS;
      return ConstantsHelper.VIEW_SPRINTS_UPDATE;
   }
   
   @PostMapping(value = ConstantsHelper.MAPPING_SPRINTS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT) @Valid SprintView view) {
      
      sprintService.save(view.translate());
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_SPRINTS;
      
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_RESOURCES_ADD + ConstantsHelper.MAPPING_PARAMETER_RESOURCE)
   public String addMember(Model model, @PathVariable int id, @PathVariable int resource ) {
      Sprint sprint = sprintService.getOne(id);
      SprintMember member = new SprintMember(sprint, resourceService.getOne(resource));
      
      sprint.getMembers().add(sprintMemberService.save(member));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprintService.save(sprint)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, sprintsHelper.listOfResourcesAvailable(sprint));
      
      return ConstantsHelper.MAPPING_SPRINTS_TAB_MEMBERS;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_RESOURCES_REMOVE + ConstantsHelper.MAPPING_PARAMETER_RESOURCE)
   public String removeMember(Model model, @PathVariable int id, @PathVariable int resource ) {
      Sprint sprint = sprintService.getOne(id);
      SprintMember member = sprintMemberService.getOne(new SprintMemberPK(sprint, resourceService.getOne(resource)));
      sprintMemberService.delete(member);
      sprint.getMembers().remove(member);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprint));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, sprintsHelper.listOfResourcesAvailable(sprint));
      
      return ConstantsHelper.MAPPING_SPRINTS_TAB_MEMBERS;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_TASKS_ADD + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String addTask(Model model, @PathVariable int id, @PathVariable int workitem ) {
      Sprint sprint = sprintService.getOne(id);
      WorkItem task = workItemService.getOne(workitem);
      task.setAssigned(DatesHelper.now());
      task.setAssignedTo(ResourcesHelper.getResource());
      sprint.getTasks().add(task);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprintService.save(sprint)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, sprintsHelper.getBacklog(sprint));
      
      return ConstantsHelper.MAPPING_SPRINTS_TAB_TASKS;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SPRINTS_UPDATE + "') and @SecurityService.isMemberOfSprint(#id)")
   @GetMapping(value = ConstantsHelper.MAPPING_SPRINTS + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_TASKS_REMOVE + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String removeTask(Model model, @PathVariable int id, @PathVariable int workitem ) {
      Sprint sprint = sprintService.getOne(id);
      WorkItem task = workItemService.getOne(workitem);
      task.setAssigned(null);
      task.setAssignedTo(null);
      sprint.getTasks().remove(task);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, new SprintView(sprintService.save(sprint)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, sprintsHelper.getBacklog(sprint));
      
      return ConstantsHelper.MAPPING_SPRINTS_TAB_TASKS;
   }
}
package pt.com.scrumify.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ContextHelper;
import pt.com.scrumify.helpers.CookiesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Controller
@RequestMapping(value = ConstantsHelper.MAPPING_CONTEXT)
public class ContextController {
   @Autowired
   private ContextHelper contextHelper;
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private TeamService teamsService;
   
   @GetMapping(ConstantsHelper.MAPPING_TEAMS + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String team(Model model, HttpServletRequest request, @PathVariable int id) {
      String referer = request.getHeader(ConstantsHelper.HTTP_REFERER);
      
      /*
       * Get team from database
       */
      Team team = this.teamsService.getOne(id);
            
      /*
       * Set active team on security context
       */
      contextHelper.setActiveTeam(team);
      
      /*
       * Save active team on cookie
       */
      cookiesHelper.save(ConstantsHelper.COOKIE_CONTEXT_TEAM, ResourcesHelper.getResource(), team.getId());
      
      /*
       * Redirect to original url
       */
      return ConstantsHelper.MAPPING_REDIRECT + referer;
   }
}
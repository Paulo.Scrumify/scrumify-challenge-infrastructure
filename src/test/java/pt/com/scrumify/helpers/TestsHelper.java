package pt.com.scrumify.helpers;

import pt.com.scrumify.database.entities.Area;

public class TestsHelper {
   public static Area expectedArea() {
      Area area = new Area();
      area.setId(0);
      area.setAbbreviation("NEW");
      area.setName("New Area");
      
      return area;
   }
}